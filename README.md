# Plugin « fil d'Ariane »

Ce plugin est destiné à faciliter la création du petit menu de navigation,
communément appelé « fil d'Ariane » et permettant de situer un objet éditorial dans un site SPIP.

Il s'emploie dans les squelettes et évite de faire des boucles compliquées pour parvenir à créer un fil d'Ariane uniforme pour tout un site.
L'idée est de déporter toute la complexité dans le plugin !

L'intégrateur appelle juste une balise `#FIL_ARIANE`,
et le plugin se charge de récupérer le type d'objet et son identifiant, à partir du contexte,
et de créer un magnifique fil d'Ariane sans avoir eu à écrire aucune ligne de code !

Par exemple, dans un squelette de type « article », et pour un article situé dans une sous_rubrique,
la balise suivante :

```
#FIL_ARIANE
```

provoquera l'affichage suivant :

```html
Racine du site > Rubrique > Sous rubrique > Titre de l'article
```


Éventuellement, si l'appel au fil d'Ariane se fait en dehors d'une boucle,
on peut préciser manuellement un type d'objet et un identifiant de la manière suivante :

```
#FIL_ARIANE{
	objet=rubrique,
	id_objet=12}
```

On peut aussi ajouter des paramètres, par exemple pour changer le niveau de départ (par défaut niveau 0 = racine du site)
ou limiter la profondeur d'affichage :

```
#FIL_ARIANE{
	objet=rubrique,
	id_objet=12}{0,3}
```

On peut aussi spécifier un titre, si le titre de l'objet n'est pas satisfaisant :


```
#FIL_ARIANE{
	titre=Promotion d'été}
```

On peut aussi spécifier un titre et un lien pour la page précédente, qui viendra s'ajouter à la hiérarchie calculée ;
le niveau de profondeur sera automatiquement baissé d'un cran dans ce cas afin d'éviter une duplication du lien précédent.

```
#FIL_ARIANE{
	titre_avant=Promotions,
	lien_avant=#URL_RUBRIQUE{14}}
```

## Utilisation

On installe le plugin.
On dispose par défaut de fils d'Ariane pour tous les objets éditoriaux standard de SPIP, qui sont inclus au format « Z » :
le fil d'Ariane d'un article, d'une rubrique, d'un auteur, d'un mot, etc.

Par exemple si un webmestre inclut la balise #FIL_ARIANE dans un squelette, à l'intérieur d'une boucle article, le squelette affiche :

```
Racine du site > Rubrique > Sous rubrique > Article
```

## Paramétrages
On peut préciser différents paramètres dans la partie « configuration ».

On peut préciser le « contenant » du fil d'Ariane:
par défaut il s'agit d'un bloc div portant la class « fil_ariane », contenant des des liens, mais cela pourrait être un bloc de liste `<ul>` avec des list items `<li>`.

On peu aussi préciser la class du contenant :
par défaut `class=fil_ariane`, mais ça pourrait être aussi `class=hierarchie`.

On peut enfin préciser le caractère d'espacement :
par défaut « > » mais ça pourrait aussi être un « | ».

## Styles

Le fil d'Ariane ainsi construit porte par défaut la class « fil_ariane ». par défaut le plugin ajoute une petite feuille de style permettant de styler les éléments. Dans les feuilles de style écrites par l'intégrateur d'autres styles pourront être définis.

## Espace privé
Le plugin « Fil d'Ariane » peut être utilisé dans l'espace privé afin de simplifier l'écriture des squelettes ou l'écriture des plugins.


----

### Note 1
Comme le suggérait Togg, j'ai écrit la doc du plugin « Fil d'Ariane » avant de commencer à faire le plugin.
Du coup celui-ci n'est pas du tout terminé en date du 19 juillet 2012.
Je le pose néanmoins sur la zone car ça m'est plus facile pour le développement.
Et ainsi il est ouvert à toute critique, néanmoins bienvenue;-)

### Note 2
L'objectif de ce plugin est de rendre le plus automatique possible la création d'un fil d'Ariane.
Au pire si le plugin n'est capable de rien trouver (pas appelé dans une boucle, aucun paramètre passé, etc.) il affichera quand même a minima la racine du site avec un lien.

On doit pouvoir aussi utiliser ce plugin pour construire un fil d'Ariane, même sans aucun objet éditorial.
Par exemple l'appel suivant devrait être possible :
```
#FIL_ARIANE{#LISTE{
	'Home','www.monsite.com',
	'Rubrique','www.monsite.com/rub',
	'Sous rubrique','www.monsite.com/rub/sousrub',
	'Ma page'
}
```
Et devrait génér ceci :

```
Home > Rubrique > Sous rubrique > Ma page
```

### Note 3

Il faut suivre 2 lignes directrices :
* 1. appel de la balise le plus simple possible ; le webmestre ne doit pas avoir à ré-écrire de boucle autour de l'appel à la balise
* 2. balise la plus générique possible, doit marcher pour tous les objets automatiquement, ou utiliser les paramètres qu'on lui passe
